This is to provision a virtualbox vm with Docker, Minikube, Helm and Kubectl installed. You can run Minikube with "vm-driver=none" inside the vm to avoid another layer of virtualization.

May need to turn off firewall again via `ufw disable` once Minikube is started if you are not able to access the dashboard outside the vm.